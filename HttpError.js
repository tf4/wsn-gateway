const util = require('util');

function HttpError(message, statusCode){
    this.message = message;
    this.statusCode = statusCode;
    this.stack = Error().stack;
}

HttpError.prototype = Object.create(Error.prototype);
HttpError.prototype.name = "HttpError";
module.exports  = HttpError;
