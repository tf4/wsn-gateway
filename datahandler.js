

var datahandler = exports = module.exports = {};

var nodestatusinfo = {};

function updateNodeStatusInfo(addr, updateinfo){
    var date = new Date();
    if(Object.prototype.toString.call(nodestatusinfo[addr]) !== '[object Object]') {
        nodestatusinfo[addr] = {};
    }
    for(var field in updateinfo) {
        nodestatusinfo[addr][field] = updateinfo[field];
    }
    nodestatusinfo[addr]["last-updated"] = date.toLocaleDateString();
}

datahandler.getNodeStatusInfo = function() {
    return nodestatusinfo;
};

datahandler.loadWSNMap = function (fromxml) {
    var result = {};
    for(var i = 0; i < fromxml.length; i++){
        var index = fromxml[i]['rime0'][0] + '.' + fromxml[i]['rime1'][0];
        var sensors = fromxml[i]['sensors'][0]['sensor'];
        var sensorlist = result[index] = {};
        if(!sensors) continue;
        for(var j = 0; j < sensors.length; j++){
            sensorlist[sensors[j].name[0]] = sensors[j].seq[0];
        }
        result[index] = sensorlist;
    }
    return result;
};

datahandler.wsnStringToJSON = function (data, nodelist, callback) {
    var result = {};
    var dataid;
    var addr;
    var updateinfo = [];
    if(data[0] == '#QRY_REQ#'){
        data[0] = 'query';
        dataid = data.slice(0,3).join("-") + "-" + data[11];
        addr = data[1] + '.' + data[2];
        result["address"] = [Number(data[1]), Number(data[2])];
        result['deluge-status'] = Boolean(data[3]);
        updateinfo["deluge-status"] = Boolean(Number(data[3]));
        result["packet-length"] = Number(data[4]);
        result["uptime"] = Number(data[5]);
        updateinfo["uptime"] = data[5];
        result["app-version"] = Number(data[6]);
        updateinfo["app-version"] = data[6];
        result["battery"] = Number(data[7]);
        updateinfo["battery"] = data[7];
        result["rssi"] = Number(data[8]);
        updateinfo["rssi"] = data[8];
        result["lqi"] = Number(data[9]);
        updateinfo["lqi"] = data[9];
        result["tx-power"] = Number(data[10]);
        updateinfo["tx-power"] = data[10];
        result["sensor-mask"] = Number(data[11]);
        updateNodeStatusInfo(addr, updateinfo);
        result["sensor-values"] = {};
        var sensors = [];
        var sensorlist = nodelist[addr];
        for(var entry in sensorlist){
            if(sensorlist.hasOwnProperty(entry))
                sensors[Number(sensorlist[entry])] = entry;
        }
        for(var j = 0; j < sensors.length; j++){
            if(result['sensor-mask'] & (1<<j)){
                result["sensor-values"][sensors[j]] = data[12 + j];
            }
        }
        callback(null, result, dataid);
    }
    else if(data[0] == '#LS#'){
        data[0] = 'sinkelfdir';
        result['stats'] = {'filecount': 0, 'totalsize': 0};
        result['files'] = {};
        var entries = data.slice(1, -1);
        var filecount = 0;
        for(var j = 0; j < entries.length; j++){
            var entry = entries[j].split(':');
            if(entry[1] == 'total'){
                result['stats']['totalsize'] = Number(entry[0])
            }
            else{
                result['files'][entry[1]] = Number(entry[0])
                filecount += 1;
            }
        }
        result['stats']['filecount'] = filecount;
        callback(null, result, data[0]);
    }
    else if(data[0] == '#NETSTAT#'){
        data[0] = 'netstat';
        addr = data[1] + '.' + data[2];
        updateinfo["app-version"] = data[3];
        updateinfo["uptime"] = data[4];
        updateinfo["deluge-status"] = Boolean(Number(data[5]));
        updateinfo["tx-power"] = data[6];
        updateinfo["battery"] = data[7];
        updateinfo["rssi"] = data[8];
        updateinfo["lqi"] = data[9];
        updateinfo["tx"] = data[10];
        updateinfo["rxGW"] = data[11];
        updateNodeStatusInfo(addr, updateinfo);
        callback(null, result, false)
    }
    else if(data[0] == '#NODECONF'){
        data[0] = 'config';
        dataid = data.slice(0,3).join("-");
        result["address"] = [data[1], data[2]];
        result["configuration"] = {"tx-power": data[4], "status-msg-rate": data[5]};
        updateinfo["tx-power"] = data[4];
        updateNodeStatusInfo(data[1] + '.' + data[2], updateinfo);
        callback(null, result, dataid);
    }
    else if(data[0] == "#UPDATE#"){
        var x, address, fileinfo;
        if(data[1].indexOf('STARTED') > -1){
            x = data[1].split(':');
            address = x[0].split('.');
            x = x[1].split('->');
            fileinfo = x[0].split('/');
            dataid = 'startdeluge-' + address.join('-') + '-' + fileinfo.join('-');
            result['address'] = address.join('.');
            updateinfo['deluge-status'] = true;
            updateNodeStatusInfo(result['address'], updateinfo);
            result['deluge-transfer'] = 'started';
            result['file'] = fileinfo[0];
            result['filesize'] = fileinfo[1];
            callback(null, result, dataid);
        }
        else if(data[1].indexOf('STOPPED') > -1){
            x = data[1].split(':');
            address = x[0].split('.');
            dataid = 'stopdeluge-' + address.join('-');
            result['address'] = address.join('.');
            updateinfo['deluge-status'] = false;
            updateNodeStatusInfo(result['address'], updateinfo);
            result['deluge-transfer'] = 'stopped';
            callback(null, result, dataid);
        }
        else if(data[1].indexOf('BUSY') > -1){
            x = data[1].split(':');
            address = x[0].split('.');
            x = x[1].split('->');
            fileinfo = x[0].split('/');
            dataid = 'delugebusy-' + address.join('-');
            result['address'] = address.join('.');
            updateinfo['deluge-status'] = true;
            updateNodeStatusInfo(result['address'], updateinfo);
            result['deluge-transfer'] = 'busy';
            result['file'] = fileinfo[0];
            result['filesize'] = fileinfo[1];
            callback(null, result, dataid);
        }
        else if(data[1].indexOf('DONE') > -1){
            x = data[1].split(':');
            address = x[0];
            updateinfo['deluge-status'] = false;
            updateNodeStatusInfo(address, updateinfo);
            callback(null, null, null);
        }
        else if(data[1].indexOf('FAILED') > -1){
            x = data[1].split(':');
            address = x[0];
            updateinfo['deluge-status'] = false;
            updateNodeStatusInfo(address, updateinfo);
            callback(null, null, null);
        }
        else
            callback(null, null, null);
    }
    else
        callback(null, null, null);
};
