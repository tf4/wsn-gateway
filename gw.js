const PORT_PREFIX = "/dev/pts/";
const DEFAULT_PORT_NUM = "0";
const DEFAULT_PORT_BDR = 115200;

var SerialPort = require("serialport");
var events = require("events");
var util = require("util");
var fs = require("fs");
var xml2js = require("xml2js");
var xmlparser = new xml2js.Parser();
var datahandler = require('./datahandler');

var gw = exports = module.exports = {};

var serialport = new SerialPort(DEFAULT_PORT_NUM, { baudrate: DEFAULT_PORT_BDR, 
                parser: SerialPort.parsers.readline('\n'), autoOpen: false });

var nodelist = {};

gw.loadXMLNetDescription = function(callback) {
    fs.readFile('./wsn_map/nodelist.xml', function (err, data) {
	    if (err) {
	        callback("Failed to read WSN map file", null);
        }
        else {
	        xmlparser.parseString(data, function (error, result) {
                if (error) {
                    callback("Failed to parse WSN map file", null);
                }
                else {
                    nodelist = datahandler.loadWSNMap(result['nodes']['node']);
                    console.log(nodelist);
                    callback(null, 'XML net description file successfully loaded');
                }
	        });
	    }
    });
};

gw.updateNetDescription = function(a0, a1, data, callback) {
    var address = a0 + '.' + a1;
    if(isNaN(Number(a0)) || isNaN(Number(a1))){
        callback('Failed to convert to xml: Invalid address type', null);
        return;
    }
    if(Object.prototype.toString.call(data) !== '[object Array]') {
        callback('Invalid description object', null);
        return;
    }
    if(Object.keys(data).length > 10) {
        callback('Description object too big', null);
        return;
    }
    var obj = {'node': {'rime0': a0, 'rime1': a1, 'sensors':{'sensor':[]}}};
    var nodelistentry = {};
    var i;
    for(i = 0; i < data.length; i++) {
        obj['node']['sensors']['sensor'].push({'name': data[i], 'seq': i});
        nodelistentry[data[i]] = i;
    }
    nodelist[address] = nodelistentry;
    console.log(nodelist);
    fs.readFile('./wsn_map/nodelist.xml', function (err, data) {
        if (err) {
            callback(null, {'status': {'xml': 'Failed to update', 'node': 'Description added'}});
        }
        else {
            xmlparser.parseString(data, function (error, result) {
                if (error) {
                    callback(null, {'status': {'xml': 'Failed to update', 'node': 'Description added'}});
                }
                else {
                    result['nodes']['node'].push(obj['node']);
                    var builder = new xml2js.Builder();
                    var xml = builder.buildObject(result);
                    fs.writeFile("./wsn_map/nodelist.xml", xml, function(err) {
                        if(err) {
                            callback(null, {'status': {'xml': 'Failed to update', 'node': 'Description added'}});
                        }
                        else
                            callback(null, {'status': {'xml': 'Updated', 'node': 'Description added'}});
                    });
                }
            });
        }
    });
};

gw.getNodeDescription = function(a0 , a1, callback){
    if(isNaN(Number(a0)) || isNaN(Number(a1))){
        callback('Invalid address type', null);
        return;
    }
    var address = a0 + '.' + a1;
    var desc = nodelist[address];
    var result = {};
    if(Object.prototype.toString.call(desc) === '[object Object]') {
        result[address] = desc;
        callback(null, result);
    }
    else
        callback('No available description', null);;
};

gw.portinfo = function(callback) {
    if(!serialport.isOpen()) callback('No open ports', null);
    else {
        callback(null, {path: serialport.path, baudrate: serialport.options.baudrate});
    }
};

gw.start = function(portnum, baudrate, callback) {
    var currentPath = serialport.path;
    var currrentBaudrate = serialport.options.baudrate;
    serialport.path = PORT_PREFIX + portnum;
    serialport.options.baudrate = baudrate;
    serialport.open(function(err){
        if(err) {
            serialport.path = currentPath;
            serialport.options.baudrate = currrentBaudrate;
            callback(err.message, null);
        }
        else {
            var message = "Port " + serialport.path + " opened successfully @" + serialport.options.baudrate;
	        gw.loadXMLNetDescription(function(error, msg) {
                if (error) {
                    callback(message + "\n" + error, null);
                }
                else {
                    callback(null, message + '\n' + msg)
                }
	        });
        }
    });
    //console.log("end of gw.start ");
};

gw.stop = function(callback) {
    serialport.close(function(err) {
        if(err) {
            callback(err.message, null);
        }
        else {
            callback(null, 'Port ' + serialport.path + ' was successfully closed');
        }
    });
};

gw.sendCommand = function (command, callback) {
    serialport.write(command + "\n", function () {
        serialport.drain(function (err) {
            if(err){
                callback(err.message);
            }
            else{
                console.log(command);
                callback(null);
            }
        });

    });
};

gw.translateSensorNames = function (a0, a1, sensorstring, callback) {
    var cmd = 'query ' + a0 + ' ' + a1 + ' ';
    var evtype = 'query-' + a0 + '-' + a1 + '-';
    var address = a0 + '.' + a1;
    if(!nodelist[address]){
        callback("Node address is not registered", null, null);
        return;
    }
    if(sensorstring == 'none') {
        cmd += '0';
        evtype += '0';
        callback(null, cmd, evtype);
    }
    else {
        var sensors = sensorstring.split('.');
        var bitmask = 0;
        for(var i = 0; i < sensors.length; i++) {
            var sensoridx = nodelist[address][sensors[i]];
            if(!isNaN(Number(sensoridx))) {
                bitmask |= (1<<Number(sensoridx))
            }
            else {
                callback("The requested sensor " +sensors[i] + " does not exist on this node", null, null);
                return;
            }
        }
        cmd += bitmask;
        evtype += bitmask;
        callback(null, cmd, evtype);
    }
};

gw.createNodeStartDelugeCmd = function (a0, a1, filename, filesize, callback) {
    var address = a0 + '.' + a1;
    if(!nodelist[address]){
        callback("Node address is not registered", null, null);
        return;
    }
    filename = filename.replace(/.ce$/, "");
    if(filename.length != 6) {
        callback("Filename length must be 6 characters", null, null);
        return;
    }
    if(isNaN(Number(filesize))) {
        callback("Invalid type of file size parameter", null, null);
        return;
    }
    if(Number(filesize) > 10000 || Number(filesize) <= 0) {
        callback("File size is zero or exceeds limit of 10kB", null, null);
        return;
    }
    var evtype = [];
    var cmd = "elfupdate " + a0 + ' ' + a1 + ' ' + filename + ' ' + filesize;
    evtype[0] = 'startdeluge-' + a0 + '-' + a1 + '-' + filename + '.ce-' + filesize;
    evtype[1] = 'delugebusy-' + a0 + a1;
    callback(null, cmd, evtype);
};

gw.createNodeStopDelugeCmd = function (a0, a1, callback) {
    var address = a0 + '.' + a1;
    if(!nodelist[address]){
        callback("Node address is not registered", null, null);
        return;
    }
    var cmd = "elfupdate " + a0 + ' ' + a1 + ' stopdlg 0';
    var evtype = 'stopdeluge-' + a0 + '-' + a1;
    callback(null, cmd, evtype);
}

gw.getNodeStatusInfo = function (addr) {
    if(addr == "all")
        return datahandler.getNodeStatusInfo();
    else{
        var nsi = datahandler.getNodeStatusInfo();
        return nsi[addr];
    }
};

function DataNotifier(){
    events.EventEmitter.call(this);
    this.notify = function(dataid, data){
        this.emit(dataid, data);
    }
}
util.inherits(DataNotifier, events.EventEmitter);

gw.dataNotifier = new DataNotifier();

serialport.on('open', function() {
    console.log("open event ");
});

serialport.on('data', function (wsndata) {
    console.log('Data: ' + wsndata);
    var data = wsndata.split(" ");
    datahandler.wsnStringToJSON(data, nodelist, function (err, result, broadcast_ev) {
        if(err){
            console.log("Corrupt data string: " + data);
        }
        else {
            if(broadcast_ev)
                gw.dataNotifier.notify(broadcast_ev, result);
        }
    });
});