var util = require('util');
var fs = require('fs-extra');
var async = require('async');
var express = require('express');
var ws = require('ws');
var wss = new ws.Server({port: 8080});
var HttpError = require('./HttpError');

var morgan = require("morgan");
var bodyParser = require("body-parser");
var multer = require("multer");
var gw = require("./gw");
var hostname;
var port;
if(process.env.IP && process.env.PORT){
    //Cloud9 configuration
    hostname = process.env.IP;
    port = process.env.PORT;
}
else {
    hostname = 'localhost';
    port = 3000;
}

var server = express();

server.use(morgan('dev'));

var serviceRouter = express.Router();
var descriptionRouter = express.Router();
descriptionRouter.use(bodyParser.json());
var elfDirRouter = express.Router();
var upload = multer({dest: './tempelfdir/', limits: {fields: 20, fileSize: 6000}}).single('elfile');
var uploadnetdesc = multer({dest: './tempelfdir/', limits: {fields: 20, fileSize: 1000}}).single('netdesc');
var uploadappdesc = multer({dest: './tempelfdir/', limits: {fields: 20, fileSize: 1000}}).single('appdesc');
//elfDirRouter.use(upload);
var nodeServiceRouter = express.Router();
nodeServiceRouter.use(bodyParser.json());
var sinkServiceRouter = express.Router();


sinkServiceRouter.route('/')
    .all(function(req, res, next){
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end("Please select a sink operation");
    });

sinkServiceRouter.route('/dir')
    .get(function (req, res, next) {
        var cmd = 'ls';
        var evtype = 'sinkelfdir';
        gw.sendCommand(cmd, function (err) {
            if(err){
                res.writeHead(503, {'Content-Type': 'text/plain'});
                res.end(err);
            }
            else {
                var timer = setTimeout(function () {
                    sinkTimedOut();
                }, 3000);
                gw.dataNotifier.on(evtype, sinkDataReceived);
                function sinkDataReceived(data) {
                    gw.dataNotifier.removeListener(evtype, sinkDataReceived);
                    clearTimeout(timer);
                    res.status(200).json(data);
                }
                function sinkTimedOut() {
                    gw.dataNotifier.removeListener(evtype, sinkDataReceived);
                    res.writeHead(400, {'Content-Type': 'text/plain'});
                    res.end("Request to node timed out");
                }
            }
        });
    })

nodeServiceRouter.route('/')
    .all(function(req, res, next){
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end("Please select a node");
    });

nodeServiceRouter.route('/getsensors/:addr0/:addr1/:sensors')
    .get(function (req, res, next) {
        handleSensorRequest(req, res);
    });

nodeServiceRouter.route('/getstatus/:addr0/:addr1?')
    .get(function (req, res, next) {
        handleNodeStatusRequest(req, res);
    });

nodeServiceRouter.route('/config/:addr0/:addr1')
    .put(function (req, res, next) {
        handleNodeConfigRequest(req, res);
    });

nodeServiceRouter.route('/deluge/start/:addr0/:addr1/:filename/:filesize')
    .get(function (req, res, next) {
        handleNodeDelugeRequest(req, res);
    });

nodeServiceRouter.route('/deluge/stop/:addr0/:addr1')
    .get(function (req, res, next) {
        handleNodeDelugeStopRequest(req, res);
    });

nodeServiceRouter.use(function (err, req, res, next) {
    var status = 500;
    if(err.statusCode)
        status = err.statusCode;
    res.status(status).send({ error: 'Something failed! ' + err.message });
});

function handleNodeDelugeRequest(req, res) {
    if(!validateNodeAddress(req.params.addr0, req.params.addr1)) {
        res.writeHead(400, {"Content-Type": "text/plain"});
        res.end("Invalid sensor address ");
        return;
    }
    gw.createNodeStartDelugeCmd(req.params.addr0, req.params.addr1, req.params.filename, req.params.filesize,
    function (err, cmd, evtype) {
        if(err){
            res.writeHead(400, {'Content-Type': 'text/plain'});
            res.end(err);
        }
        else{
            gw.sendCommand(cmd, function (err) {
                if(err) {
                    res.writeHead(503, {'content-Type': 'text/plain'});
                    res.end(err);
                }
                else {
                    var timer = setTimeout(function () {
                        wsnTimedOut();
                    }, 3000);
                    gw.dataNotifier.on(evtype[0], wsnDataReceived);
                    gw.dataNotifier.on(evtype[1], wsnDataReceived);
                    function wsnDataReceived(data) {
                        gw.dataNotifier.removeListener(evtype[0], wsnDataReceived);
                        gw.dataNotifier.removeListener(evtype[1], wsnDataReceived);
                        clearTimeout(timer);
                        //res.writeHead(200, {'Content-Type': 'text/plain'});
                        res.status(200).json(data);
                        //res.end(data);
                    }
                    function wsnTimedOut() {
                        gw.dataNotifier.removeListener(evtype[0], wsnDataReceived);
                        gw.dataNotifier.removeListener(evtype[1], wsnDataReceived);
                        res.writeHead(400, {'Content-Type': 'text/plain'});
                        res.end("Request to node timed out");
                    }
                }
            })
        }
    })
}

function handleNodeDelugeStopRequest(req, res) {
    if(!validateNodeAddress(req.params.addr0, req.params.addr1)) {
        res.writeHead(400, {"Content-Type": "text/plain"});
        res.end("Invalid sensor address ");
        return;
    }
    gw.createNodeStopDelugeCmd(req.params.addr0, req.params.addr1, function (err, cmd, evtype) {
            if(err){
                res.writeHead(400, {'Content-Type': 'text/plain'});
                res.end(err);
            }
            else{
                gw.sendCommand(cmd, function (err) {
                    if(err) {
                        res.writeHead(503, {'content-Type': 'text/plain'});
                        res.end(err);
                    }
                    else {
                        var timer = setTimeout(function () {
                            wsnTimedOut();
                        }, 3000);
                        gw.dataNotifier.on(evtype, wsnDataReceived);
                        function wsnDataReceived(data) {
                            gw.dataNotifier.removeListener(evtype, wsnDataReceived);
                            clearTimeout(timer);
                            //res.writeHead(200, {'Content-Type': 'text/plain'});
                            res.status(200).json(data);
                            //res.end(data);
                        }
                        function wsnTimedOut() {
                            gw.dataNotifier.removeListener(evtype, wsnDataReceived);
                            res.writeHead(400, {'Content-Type': 'text/plain'});
                            res.end("Request to node timed out");
                        }
                    }
                })
            }
        })
}

function handleNodeConfigRequest(req, res) {
    if(!validateNodeAddress(req.params.addr0, req.params.addr1)) {
        res.writeHead(400, {"Content-Type": "text/plain"});
        res.end("Invalid sensor address ");
        return;
    }
    if(Object.prototype.toString.call(req.body) !== '[object Object]'){
        res.writeHead(400, {"Content-Type": "text/plain"});
        res.end("No configuration parameters ");
        return;
    }
    var bitmask = 0;
    var txpower = '';
    var statusmsgrate = '';
    if(req.body['tx-power']) {
        if (isNaN(Number(req.body['tx-power']))) {
            res.writeHead(400, {"Content-Type": "text/plain"});
            res.end("Invalid type of parameter tx-power ");
            return;
        }
        bitmask |= 1;
        txpower = ' ' + req.body['tx-power'];
    }
    if(req.body['status-msg-rate']){
        if(isNaN(Number(req.body['status-msg-rate']))){
            res.writeHead(400, {"Content-Type": "text/plain"});
            res.end("Invalid type of parameter status-msg-rate ");
            return;
        }
        bitmask |= 2;
        statusmsgrate = ' ' + req.body['status-msg-rate'];
    }
    if(bitmask == 0){
        res.writeHead(400, {"Content-Type": "text/plain"});
        res.end("No configuration parameters provided ");
        return;
    }
    var cmd = 'nodeconf ' + req.params.addr0 + ' ' + req.params.addr1 + ' ';
    cmd += bitmask + txpower + statusmsgrate;
    var evtype = 'config-' + req.params.addr0 + '-' + req.params.addr1;
    gw.sendCommand(cmd, function (err) {
        if(err) {
            res.writeHead(503, {'content-Type': 'text/plain'});
            res.end(err);
        }
        else {
            var timer = setTimeout(function () {
                wsnTimedOut();
            }, 3000);
            gw.dataNotifier.on(evtype, wsnDataReceived);
            function wsnDataReceived(data) {
                gw.dataNotifier.removeListener(evtype, wsnDataReceived);
                clearTimeout(timer);
                //res.writeHead(200, {'Content-Type': 'text/plain'});
                res.status(200).json(data);
                //res.end(data);
            }
            function wsnTimedOut() {
                gw.dataNotifier.removeListener(evtype, wsnDataReceived);
                res.writeHead(400, {'Content-Type': 'text/plain'});
                res.end("Request to node timed out");
            }
        }
    });
}

function handleSensorRequest(req, res) {
    if(!validateNodeAddress(req.params.addr0, req.params.addr1)) {
        res.writeHead(400, {"Content-Type": "text/plain"});
        res.end("Invalid sensor address ");
        return;
    }
    gw.translateSensorNames(req.params.addr0, req.params.addr1, req.params.sensors, function (err, cmd, evtype) {
        if(err){
            res.writeHead(404, {'Content-Type': 'text/plain'});
            res.end(err);
        }
        else {
            gw.sendCommand(cmd, function (err) {
                if(err) {
                    res.writeHead(503, {'content-Type': 'text/plain'});
                    res.end(err);
                }
                else {
                    var timer = setTimeout(function () {
                        wsnTimedOut();
                    }, 3000);
                    gw.dataNotifier.on(evtype, wsnDataReceived);
                    function wsnDataReceived(data) {
                        gw.dataNotifier.removeListener(evtype, wsnDataReceived);
                        clearTimeout(timer);
                        //res.writeHead(200, {'Content-Type': 'text/plain'});
                        res.status(200).json(data);
                        //res.end(data);
                    }
                    function wsnTimedOut() {
                        gw.dataNotifier.removeListener(evtype, wsnDataReceived);
                        res.writeHead(400, {'Content-Type': 'text/plain'});
                        res.end("Request to node timed out");
                    }
                }
            });
        }
    });
}

function handleNodeStatusRequest(req, res) {
    if(req.params.addr0 == 'all' && !req.params.addr1) {
        res.status(200).json(gw.getNodeStatusInfo('all'));
        return;
    }
    if(!validateNodeAddress(req.params.addr0, req.params.addr1)) {
        res.writeHead(400, {"Content-Type": "text/plain"});
        res.end("Invalid sensor address ");
        return;
    }
    res.status(200).json(gw.getNodeStatusInfo(req.params.addr0 + '.' + req.params.addr1));
}

function validateNodeAddress(a0, a1) {
    if(isNaN(Number(a0)) || isNaN(Number(a1)))
        return false;
    return !(Number(a0) < 0 || Number(a0) > 255 || Number(a1) < 0 || Number(a1) > 255);

}

elfDirRouter.route('/:fname?')
    .get(function (req, res, next) {
        if(req.params.fname != 'dir')
            return next(new HttpError('Invalid GET request', 404));
        fs.readdir('./elfdir/', function (err, files) {
            if(err)
                return next(new HttpError(err.message, 502));
            var result = {'files': {}, 'total': 0, 'size': 0};
            console.log(files[0]);
	    async.map(files.map(function(item){return './elfdir/' + item}), fs.stat, function(err, results){
	      if(err)
		return HttpError(new HttpError(err.message, 502));
	      for(var i = 0; i < results.length; i++) {
		//console.log(results);
		result['files'][files[i]] = results[i]['size'];
		result['size'] += Number(results[i]['size']);
	      }
	      result['total'] = files.length;
	      res.status(200).json(result);
	    });
        });
    })
    .post(function(req, res, next){
        if(!/^[a-zA-Z0-9]{6}$/.test(req.params.fname)) {
            return next(new HttpError('Filename must be exactly 6 character long. Only numbers and latin letters allowed', 400));
        }
        upload(req, res, function (err) {
            if (err) {
                // An error occurred when uploading
                return next(new HttpError(err.message, 400));
            }
            console.log(req.file);
            //Do some extra checks
            if(!req.file) {
                return next(new HttpError("Invalid POST request", 400));
            }
            if(req.file.fieldname != 'elfile'){
                fs.unlink('./' + req.file.path);
                return next(new HttpError("No file", 400));
            }
            if(req.file.size == 0){
                fs.unlink('./' + req.file.path);
                return next(new HttpError("File of size 0", 400));
            }
            // Everything went fine
            fs.copy(req.file.path, './elfdir/' + req.params.fname + '.ce', function (err) {
                if(err){
                    fs.unlink('./' + req.file.path);
                    return next(new HttpError(err.message, 502));
                }
                fs.unlink('./' + req.file.path);
                res.status(200).json({"file": req.file.originalname, "status": "uploaded as " + req.params.fname});
            })
        })
    })
    .delete(function (req, res, next) {
        if(!/^[*]?[a-zA-Z0-9]{1,6}[*]?$/.test(req.params.fname)) {
            return next(new HttpError('Invalid file or pattern', 400));
        }
        fs.unlink('./elfdir/' + req.params.fname + '.ce', function (err) {
            if(err){
                return next(new HttpError(err.message, 400));
            }
            res.status(200).json({'file': req.params.fname, 'status': 'deleted'});
        })
    });

elfDirRouter.use(function (err, req, res, next) {
    var status = 500;
    if(err.statusCode)
        status = err.statusCode;
    res.status(status).send({ error: 'Something failed! ' + err.message });
});

descriptionRouter.route('/')
    .all(function(req, res, next){
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end("Please select a description operation");
    });
    
descriptionRouter.route('/network')
    .get(function(req, res, next) {
        var file = __dirname + '/wsn_map/nodelist.xml';
        fs.stat(file, function (err, stats) {
            if(err)
                return next(new HttpError('Failed to find a valid description file', 400))
            res.download(file); // Set disposition and send it.
        });
    })
    .post(function(req, res, next){
	    uploadnetdesc(req, res, function (err) {
            if (err) {
                // An error occurred when uploading
                return next(new HttpError(err.message, 400));
            }
            console.log(req.file);
            //Do some extra checks
            if(!req.file) {
                return next(new HttpError("Invalid POST request", 400));
            }
            if(req.file.fieldname != 'netdesc'){
                fs.unlink('./' + req.file.path);
                return next(new HttpError("No file", 400));
            }
            if(req.file.size == 0){
                fs.unlink('./' + req.file.path);
                return next(new HttpError("File of size 0", 400));
            }
            // Everything went fine
            fs.copy(req.file.path, './wsn_map/nodelist.xml', function (err) {
                if(err){
                    fs.unlink('./' + req.file.path);
                    return next(new HttpError(err.message, 502));
                }
                gw.loadXMLNetDescription(function(err, msg){
                    fs.unlink('./' + req.file.path);
                    if(err)
                    return next(new HttpError(err, 502))
                    res.status(200).json({"file": req.file.originalname, "status": "uploaded file, net description updated"});
                });
            })
        });
    });

descriptionRouter.route('/network/:addr0/:addr1')
    .get(function (req, res, next) {
        gw.getNodeDescription(req.params.addr0, req.params.addr1, function(err, result){
            if(err)
                return next(new HttpError(err, 400));
            res.status(200).json(result);
        })
    })
    .put(function (req, res, next) {
        if(!validateNodeAddress(req.params.addr0, req.params. addr1))
            return next(new HttpError('Invalid address parameters', 400));
        gw.updateNetDescription(req.params.addr0, req.params.addr1, req.body['sensors'], function (err, result) {
            if(err)
                return next(new HttpError(err, 400));
            res.status(200).json({'node': result});
        })
    });
    
descriptionRouter.route('/app/:appid/:desctype')
    .get(function(req, res, next) {
        var file = __dirname + '/appdescriptions/' + req.params.desctype + '/' + req.params.appid + '.xml';
        fs.stat(file, function (err, stats) {
            if(err)
                return next(new HttpError('Failed to find a valid description file', 400))
            res.download(file); // Set disposition and send it.
        });
    })
    .post(function (req, res, next) {
        if(isNaN(Number(req.params.appid)) || Number(req.params.appid) > 66)
            return next(new HttpErorr('Invalid application id', 400));
        var targetfile = './appdescriptions/';
        if(req.params.desctype == 'cmd')
            targetfile +=  'cmd/' + req.params.appid + '.xml';
        else if(req.params.desctype == 'data')
            targetfile += 'data/' + req.params.appid + '.xml';
        else
            return next(new HttpError('Invalid POST request', 404));
        uploadappdesc(req, res, function (err) {
            if(err){
                return next(new HttpError(err.message, 400));
            }
            if(!req.file) {
                return next(new HttpError("Invalid POST request", 400));
            }
            if(req.file.fieldname != 'appdesc'){
                fs.unlink('./' + req.file.path);
                return next(new HttpError("No file", 400));
            }
            if(req.file.size == 0){
                fs.unlink('./' + req.file.path);
                return next(new HttpError("File of size 0", 400));
            }
            // Everything went fine
            fs.copy(req.file.path, targetfile, function (err) {
                if(err){
                    fs.unlink('./' + req.file.path);
                    return next(new HttpError(err.message, 502));
                }
                gw.loadXMLNetDescription(function(err, msg){
                    fs.unlink('./' + req.file.path);
                    if(err)
                        return next(new HttpError(err, 502))
                    res.status(200).json({"file": req.file.originalname, "status": "uploaded file, app " + req.params.desctype + " description updated"});
                });
            });
        });
    });

descriptionRouter.use(function (err, req, res, next) {
    var status = 500;
    if(err.statusCode)
        status = err.statusCode;
    res.status(status).send({ error: 'Something failed! ' + err.message });
});

//serviceRouter.use(bodyParser);
serviceRouter.route('/')
    .all(function(req, res, next){
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end("Please select a service");
});

serviceRouter.route('/openport/:portnum?/:baudrate?')
    .get(function(req, res, next){
        var portnum = req.params.portnum;
        var bdrate = req.params.baudrate;
        res.writeHead(200, {'Content-Type': 'text/plain'});
        if(!portnum) {
            portnum = "0";
        }
        if(!bdrate){
            bdrate = 115200;
        }
        gw.start(portnum, bdrate, function(err, result) {
            if(err) {
                res.end(err);
            }
            else
                res.end(result);
        });
        //console.log('end of openport '+ portnum);
    });

serviceRouter.route('/closeport')
    .get(function(req, res, next){
        gw.stop(function(err, data) {
            if(err) {
                res.writeHead(500, {'Content-Type': 'text/plain'});
                res.end(err);
            }
            else {
                res.writeHead(200, {'Content-Type': 'text/plain'});
                res.end(data);
            }
        });
    });

serviceRouter.route('/whichport')
    .get(function(req, res, next){
        gw.portinfo(function(err, data) {
            if(err) res.status(500).json({'error': err});
            else res.status(200).json(data);
        });
    });


serviceRouter.use('/description', descriptionRouter);
serviceRouter.use('/sink', sinkServiceRouter);
serviceRouter.use('/node', nodeServiceRouter);
serviceRouter.use('/elf', elfDirRouter);
server.use('/service', serviceRouter);
server.listen(port, hostname, function(){
    console.log("Server started on " + hostname + " port " + port);
});

wss.on('connection', function connection(ws) {
    //console.log(util.inspect(ws));
    console.log(wss.clients)
    ws.on('message', function incoming(message) {
        console.log('received: %s', message);
    });

    ws.send('something');
});